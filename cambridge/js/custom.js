//MAGNIFIC POPUP
let arrsrc = [
    "https://www.youtube.com/embed/7Q6SyXnAhIA",
    "https://www.youtube.com/embed/54gAEg7lS5Y",
    "https://www.youtube.com/embed/p7QErWW4yok"
];
let arrtext = [
    "Cô bé Phương Uyên có uớc mơ trở thành một nghệ sĩ trang điểm như thần tượng Michelle Phan. Vì thế, cô bé luôn mày mò tìm kiếm những thông tin liên quan từ tiếng Anh thông qua Youtube, Blog... để biến uớc mơ của mình thành hiện thực",
    "Quế Thy mong uớc được trở thành 1 bác sĩ. Cô bé cố gắng học tiếng Anh thật tốt để xin học bổng đi du học, làm quen với nhiều bạn mới, tự tin giao tiếp trong môi trường quốc tế và hiện thực hoá ước mơ của mình",
    "Phạm Lê Linh Đan là cô bé nhỏ nhắn nhưng có uớc mơ lớn. Cô bé luôn không ngừng cố gắng học tập thật tốt. Đặc biệt là tiếng Anh. Bởi với em: tiếng anh là chìa khoá mở ra cơ hội và thành công trong tương lai",
];
let arrimg = [
    "images/icon/icon-pu.png",
    "images/icon/icon-qt.png",
    "images/icon/icon-ld.png",
];
let arrname = [
    "Phương Uyên",
    "Quế Thy",
    "Linh Đan"
]
function showVideos(i) {
    let src = arrsrc[i];
    let text = arrtext[i];
    let img = arrimg[i];
    let name = arrname[i];
    return  "<div class='col-md-7'>" +
                "<iframe class='phuong-uyen' src='"+src+"' frameborder='0' allowfullscreen></iframe>" +
            "</div>" +
            "<div class='col-md-5'>" +
                "<img class='icon-pu' src='"+img+"'><span id='phuong-uyen'>"+name+"</span>" +
                "<p class='pu-dream'>"+text+"</p>" +
            "</div>"
};
